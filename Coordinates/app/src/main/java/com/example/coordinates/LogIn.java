package com.example.coordinates;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LogIn extends AppCompatActivity {

    EditText email;

    EditText password;
    Button button;


    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        email = (EditText) findViewById(R.id.editText);
        password = (EditText) findViewById(R.id.editText2);
        button = (Button) findViewById(R.id.button);

        firebaseAuth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null)
                {

                }
            }
        };
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String eMail = email.getText().toString();
                String passwd = password.getText().toString();
                if (eMail.isEmpty() && passwd.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Complete fields.", Toast.LENGTH_SHORT).show();
                } else if (eMail.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Complete email field.", Toast.LENGTH_SHORT).show();
                } else if (passwd.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Complete password field.", Toast.LENGTH_SHORT).show();
                }

                if ((!eMail.isEmpty()) && (!passwd.isEmpty())) {
                    firebaseAuth.signInWithEmailAndPassword(eMail, passwd).addOnCompleteListener( new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful())
                            {
                                Toast.makeText(getApplicationContext(),"Sign in not successfull.", Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(),"Sign in successfully.", Toast.LENGTH_SHORT).show();
                                Intent myIntent = new Intent(LogIn.this, GPSTracker.class);
                                myIntent.putExtra("email", eMail);
                                startActivity(myIntent);
                            }


                        }
                    });


                }

            }
        });

    }


    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(authStateListener);
    }
}



