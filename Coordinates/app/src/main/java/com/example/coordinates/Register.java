package com.example.coordinates;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextPaint;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Register extends AppCompatActivity {

    EditText editText, editText2, editText3, editText7;
    Button button;
    Member member;
    FirebaseAuth firebaseAuth;
    DatabaseReference reference;
    CheckBox checkBox;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        editText = (EditText)findViewById(R.id.editText3);
        editText2 = (EditText)findViewById(R.id.editText4);
        editText3 = (EditText)findViewById(R.id.editText5);
        editText7 = (EditText)findViewById(R.id.editText7);
        button = (Button)findViewById(R.id.button4);

        member = new Member();
        reference = FirebaseDatabase.getInstance().getReference().child("Member");
        firebaseAuth = FirebaseAuth.getInstance();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String mail = editText.getText().toString();
                String pass = editText2.getText().toString();
                String repass = editText3.getText().toString();
                final String name = editText7.getText().toString();

                if (mail.isEmpty() && pass.isEmpty() && repass.isEmpty())
                {
                    Toast.makeText(getApplicationContext(), "Complete fields.",Toast.LENGTH_SHORT).show();
                }
                else if (mail.isEmpty())
                {
                    Toast.makeText(getApplicationContext(), "Complete mail field.",Toast.LENGTH_SHORT).show();
                }
                else if (pass.isEmpty())
                {
                    Toast.makeText(getApplicationContext(), "Complete password field.",Toast.LENGTH_SHORT).show();
                }
                else if (repass.isEmpty())
                {
                    Toast.makeText(getApplicationContext(), "Retype your password.",Toast.LENGTH_SHORT).show();
                }
                if ((!(mail.isEmpty())) && (!(pass.isEmpty())) && (!repass.isEmpty()))
                {
                    if (pass.equals(repass))
                    {
                        if (pass.length() <6)
                        {
                            Toast.makeText(Register.this,"Password must contain at least 6 characters.", Toast.LENGTH_LONG).show();
                        }
                        else {
                            firebaseAuth.createUserWithEmailAndPassword(mail, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        //user have been saved
                                        Toast.makeText(Register.this, "Successfully registered", Toast.LENGTH_SHORT).show();
                                        Intent myIntent = new Intent(Register.this, MainActivity.class);

                                    } else {
                                        Toast.makeText(Register.this, "Unsuccessfully registered", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Password missmatch.",Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
    }
}
