package com.example.coordinates;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;


public class GPSTracker extends AppCompatActivity implements LocationListener {

    EditText editText6, editText8;
    Member member;
    Button sendData;
    double lat;
    double lng;
    private LocationManager locationManager;
    private String provider;

    String mailAdress;
    DatabaseReference reference;
    int index=0;
    private String Latitude;
    private String Longitude;
    @Overridef
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gpstracker2);
        reference = FirebaseDatabase.getInstance().getReference().child("Users");
        editText6 = (EditText) findViewById(R.id.editText6);
        editText8 = (EditText) findViewById(R.id.editText8);
        sendData = (Button) findViewById(R.id.button5);
        member = new Member();

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        sendLocation();

        //Send location button, not start
        sendData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendLocation();
                Intent myIntent = getIntent();
                String email = myIntent.getStringExtra("email");
                email = email.replace("."," ");
                reference = FirebaseDatabase.getInstance().getReference().child("Users/"+email+"/Coordinates");
                reference.child(String.valueOf(index)+"/Latitude").setValue(lat);
                reference.child(String.valueOf(index)+"/Longitude").setValue(lng);
                index++;
                Toast.makeText(GPSTracker.this, "Data uploaded successfully",Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void sendLocation()
    {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
            }
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Permission not granted!",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        Location location = locationManager.getLastKnownLocation(provider);
        if (location != null) {
            System.out.println("Provider " + provider + " has been selected.");
            Toast.makeText(this, "prov obtained",
                    Toast.LENGTH_SHORT).show();
            onLocationChanged(location);
        } else {
            Toast.makeText(this, "error",
                    Toast.LENGTH_SHORT).show();
            System.out.println("not available");
        }

        locationManager.requestLocationUpdates(provider,3*60*1000 , 50, this);
    }

    @Override
    public void onLocationChanged(Location location) {

        try {
            lat = (location.getLatitude());
            lng = (location.getLongitude());
            System.out.println("location obtained");
            editText6.setText(Double.toString(lat));
            editText8.setText(Double.toString(lng));
            Toast.makeText(this, "location obtained",Toast.LENGTH_SHORT).show();

        }
        catch (Exception e)
        {

        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
