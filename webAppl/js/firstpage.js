///////-------------------------------------F I R E B A S E  C O N N E C T I O N----------------------------------------------
var firebaseConfig = {
    apiKey: "AIzaSyCgYfyjProxRPq9MYMVsUZps8SHZQ9o1Dw",
    authDomain: "gpslocation-cf022.firebaseapp.com",
    projectId: "gpslocation-cf022",
    storageBucket: "gpslocation-cf022.appspot.com",
    messagingSenderId: "833617545964",
    appId: "1:833617545964:web:07ef06f564bc97983933c9",
    measurementId: "G-8J5QX4SYP7"
};
const auth = firebase.auth();
var database = firebase.database();

/*Brif:
    function implemented in order to check usesr's info. 
    If data are correct, proceed to login. Otherwiser, return allert 
*/
function logInButton() {
    var email = document.getElementById('emailAuth').value;
    let password = document.getElementById('passwordAuth').value;
    firebase.auth().signInWithEmailAndPassword(email, password)
        .then((user) => {
            if (email == "admin@utcn.ro") {
                window.location.href = 'adminappl.html';
                return;
            }
            redirectToMainAppl(email);
        })
        .catch((error) => {
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(error.message);
        });
}

/*Brif:
    function implemented in order to redirect user to register page 
*/
function goToRegButton() {
    window.location.href = 'registration.html';
}

/*Brif:
    function implemented in order to check usesr's info. 
    If data are correct, proceed to register -> send data to FB + return user to auth page 
*/
function registerButton() {
    let fname = document.getElementById('fnameReg').value;
    let lname = document.getElementById('lnameReg').value;
    let email = document.getElementById('emailReg').value;
    let password = document.getElementById('passwordReg').value;
    let repassword = document.getElementById('repasswordReg').value;
    let emailNew = null;
    if (password.length < 6) {
        alert("Password must be at least 6 characters");
        return;
    }
    if (password == repassword && lname.length > 0 && fname.length > 0) {

        firebase.auth().createUserWithEmailAndPassword(email, password) //creeare user cu email si parola
            .then((user) => { //daca s-a creeat cu succes
                email = email.replace(".", " ");
                console.log(email);
                firebase.database().ref('Users/' + email).update({
                    FirstName: fname,
                    LastName: lname
                });
                window.location.href = "index.html";
            })
            .catch((error) => { //daca creearea nu a putut fi realizata
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(error.message);
            });

    }
}

/*Brief:
    Redirect to main application - control dashboard
*/
function redirectToMainAppl(email) {

    let query = "?" + email;
    window.location.href = "mainappl.html" + query; //mainapplication.html?andreibranga@gmail
}


function passRecover() {
    document.getElementById("mainDiv").style.visibility = "hidden";
    document.getElementById("passRecover").style.visibility = "visible";
}

function resendPass() {
    document.getElementById("mainDiv").style.visibility = "visible";
    document.getElementById("passRecover").style.visibility = "hidden";
    let email = document.getElementById("emailRecov").value;
    console.log(email);
    firebase.auth().sendPasswordResetEmail(email);
}