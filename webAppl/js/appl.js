///////-------------------------------------F I R E B A S E  C O N N E C T I O N----------------------------------------------
var firebaseConfig = {
    apiKey: "AIzaSyCgYfyjProxRPq9MYMVsUZps8SHZQ9o1Dw",
    authDomain: "gpslocation-cf022.firebaseapp.com",
    projectId: "gpslocation-cf022",
    storageBucket: "gpslocation-cf022.appspot.com",
    messagingSenderId: "833617545964",
    appId: "1:833617545964:web:07ef06f564bc97983933c9",
    measurementId: "G-8J5QX4SYP7"
};
const auth = firebase.auth();
var database = firebase.database();
var refUsers = database.ref("Users");

function loadPageData() {
    var deviceList = document.getElementById("deviceList");
    let option = document.createElement("option");
    let myKeys = [];

    deviceList.options[deviceList.options.length] = new Option("Choose one user");
    refUsers.on('value', function(snapshot) {
        let i, L = deviceList.options.length - 1;
        myKeys = [];
        selectedDeviceIndex = deviceList.selectedIndex;
        //create array from all users
        for (var index in snapshot.val()) {
            if (index != "admin@utcn ro")
                myKeys.push(index);
            console.log(myKeys);
        }
        for (i = L; i >= 0; i--) {
            deviceList.remove(i);
        }
        for (index in myKeys) {
            option.innerHTML = myKeys[index];
            option.value = myKeys[index];
            deviceList.options[deviceList.options.length] = new Option(option.innerHTML, option.value);
        }
        deviceList.selectedIndex = selectedDeviceIndex;
    });

}

function getUserData() {
    let selectedKey = document.getElementById("deviceList").value;
    refUsers.on('value', function(snapshot) {
        document.getElementById('fnameSh').value = snapshot.val()[selectedKey]["FirstName"];
        document.getElementById('lnameSh').value = snapshot.val()[selectedKey]["LastName"];
    });
    showMap();
}

function editUserData() {
    let selectedKey = document.getElementById("deviceList").value;
    let fname = document.getElementById('fnameSh').value;
    let lname = document.getElementById('lnameSh').value;
    firebase.database().ref('Users/' + selectedKey).update({
        FirstName: fname,
        LastName: lname
    });
}

function deleteUser() {
    let selectedKey = document.getElementById("deviceList").value;
    let userRef = this.database.ref('Users/' + selectedKey);
    userRef.remove()
}

function showMap() {
    let selectedKey = document.getElementById("deviceList").value;
    let dataCollected;
    let locations = [];
    refUsers.on('value', function(snapshot) {
        for (index in snapshot.val()[selectedKey]["Coordinates"]) {
            dataCollected = [index, snapshot.val()[selectedKey]["Coordinates"][index]["Latitude"], snapshot.val()[selectedKey]["Coordinates"][index]["Longitude"], 4];
            locations.push(dataCollected);
        }
    });

    console.log(locations);
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: new google.maps.LatLng(47.78895236333147, 22.870186315944018),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
        });

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }
}