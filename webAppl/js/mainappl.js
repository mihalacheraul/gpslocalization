///////-------------------------------------F I R E B A S E  C O N N E C T I O N----------------------------------------------
var firebaseConfig = {
    apiKey: "AIzaSyCgYfyjProxRPq9MYMVsUZps8SHZQ9o1Dw",
    authDomain: "gpslocation-cf022.firebaseapp.com",
    projectId: "gpslocation-cf022",
    storageBucket: "gpslocation-cf022.appspot.com",
    messagingSenderId: "833617545964",
    appId: "1:833617545964:web:07ef06f564bc97983933c9",
    measurementId: "G-8J5QX4SYP7"
};
const auth = firebase.auth();
var database = firebase.database(); //gpslocation-cf022
var refUsers = database.ref("Users");

var userMail = decodeURIComponent(window.location.search).substring(1); //email
userMail = userMail.replace(".", " ");



function loadPageData() {
    refUsers.on('value', function(snapshot) {

        document.getElementById("userName").innerHTML = snapshot.val()[userMail]["FirstName"];
        document.getElementById("fnameMain").value = snapshot.val()[userMail]["FirstName"];
        document.getElementById("lnameMain").value = snapshot.val()[userMail]["LastName"];


    });
}
usermail = "branganadrei@ga"

function editUserData() {
    let fname = document.getElementById('fnameMain').value;
    let lname = document.getElementById('lnameMain').value;
    refUsers.on('value', function(snapshot) {

        firebase.database().ref('Users/' + userMail).update({ // Users/branganadrei@ga
            FirstName: fname,
            LastName: lname
        });


    });
}

function deleteUser() {
    refUsers.on('value', function(snapshot) {


        let userRef = this.database.ref('Users/' + userMail);
        userRef.remove()


    });
}

function showMap() {

    let dataCollected;
    let locations = [];
    // [index, latitudinePunct, longitudinePunct, 4]
    refUsers.on('value', function(snapshot) {
        for (index in snapshot.val()[userMail]["Coordinates"]) {
            dataCollected = [index, snapshot.val()[userMail]["Coordinates"][index]["Latitude"], snapshot.val()[userMail]["Coordinates"][index]["Longitude"], 4];
            locations.push(dataCollected);
            console.log(locations);

        }
    });
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: new google.maps.LatLng(47.78895236333147, 22.870186315944018),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
        });

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(locations[i][0]); //labelu de pe marker
                infowindow.open(map, marker);
            }
        })(marker, i));
    }
}